package controller;

import java.util.Scanner;

import controller.commandparser.InputParser;
import exceptions.InvalidCommandException;

public class Main {

	public static void main(String[] args) {
		Scanner scanInput = new Scanner(System.in);
		InputParser parser = new InputParser(scanInput);
		try {
			parser.parseUserInput();
		} catch (InvalidCommandException e) {
			System.out.println(e);
		}
		scanInput.close();
	}

}
