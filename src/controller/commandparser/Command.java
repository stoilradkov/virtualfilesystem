package controller.commandparser;

import model.FileSystem;

abstract class Command {
	FileSystem fileSystem;

	public Command(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
	}

	public String getCurrentLocation() {
		return fileSystem.getCurrentLocation();
	}

	public abstract int executeCommand(String information);

	public boolean correctInput(String information) {
		return information != null;
	}
}