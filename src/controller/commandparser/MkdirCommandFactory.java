package controller.commandparser;

import java.util.HashMap;

import model.FileSystem;

class MkdirCommandFactory {
	private HashMap<String, MkdirCommandType> mkdirCommandTypes;
	private FileSystem fileSystem;

	MkdirCommandFactory(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
		mkdirCommandTypes = new HashMap<>();
	}

	public MkdirCommandType getMkdirCommand(String information) {
		return mkdirCommandTypes.getOrDefault(information, new DefaultMkdirCommand(fileSystem));
	}
}