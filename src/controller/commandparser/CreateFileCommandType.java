package controller.commandparser;

import exceptions.DuplicateVirtualFileNameException;

abstract class CreateFileCommandType {
	abstract void executeCreateFileCommand(String information) throws DuplicateVirtualFileNameException;

	protected boolean correctInput(String information) {
		return (information != null && !information.equals(""));
	}

}
