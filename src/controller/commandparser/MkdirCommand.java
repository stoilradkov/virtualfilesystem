package controller.commandparser;

import model.FileSystem;

class MkdirCommand extends Command {
	public MkdirCommand(FileSystem fileSystem) {
		super(fileSystem);
	}

	@Override
	public int executeCommand(String information) {
		if (correctInput(information)) {
			MkdirCommandFactory mkdirCommandFactory = new MkdirCommandFactory(this.fileSystem);
			StringBuilder option = new StringBuilder();
			int i = 0;
			while (i < information.length() && information.charAt(i) != ' ') {
				option.append(information.charAt(i));
				i++;
			}
			MkdirCommandType mkdirType = mkdirCommandFactory.getMkdirCommand(option.toString());
			mkdirType.executeMkdirCommand(information);
			return 1;
		}
		System.out.println("The syntax of the command is incorrect");
		return 0;
	}

	@Override
	public boolean correctInput(String information) {
		return information != null;
	}
}
