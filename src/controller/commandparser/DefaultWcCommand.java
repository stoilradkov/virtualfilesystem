package controller.commandparser;

import model.File;
import model.FileSystem;

class DefaultWcCommand extends WcCommandType {
	private FileSystem fileSystem;

	public DefaultWcCommand(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
	}

	private String getContent(String content) {
		if (content.matches(".*txt")) {
			File atLocation = fileSystem.findFile(content);
			if (atLocation == null || atLocation.isRemoved()) {
				return content;
			}
			return atLocation.getContent();
		} else {
			return content;
		}
	}

	@Override
	protected boolean correctInput(String information) {
		return information != null;
	}

	@Override
	int executeWcCommand(String information) {
		if (correctInput(information)) {
			String content;
			content = getContent(information).trim();
			int numberOfWords = 1;
			for (int i = 0; i < content.length(); i++) {
				if (content.charAt(i) == ' ') {
					numberOfWords++;
				}
			}
			System.out.println(numberOfWords);
			return numberOfWords;
		}
		System.out.println("The syntax of the command is incorrect");
		return 0;
	}
}
