package controller.commandparser;

import exceptions.NoSuchVirtualFileException;

abstract class RmCommandType {
	abstract void executeRmCommand(String information) throws NoSuchVirtualFileException;

	protected boolean correctInput(String information) {
		return (information != null && !information.equals(""));
	}

}
