package controller.commandparser;

import model.FileSystem;

class LsCommand extends Command {
	private LsCommandFactory lsCommandFactory;

	public LsCommand(FileSystem fileSystem) {
		super(fileSystem);
		lsCommandFactory = new LsCommandFactory(fileSystem);
	}

	@Override
	public int executeCommand(String information) {
		LsCommandType lsType = lsCommandFactory.getLsCommand(information);
		lsType.executeLsCommand(information);
		return 0;
	}
}
