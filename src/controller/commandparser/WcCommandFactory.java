package controller.commandparser;

import java.util.HashMap;

import model.FileSystem;

class WcCommandFactory {
	private HashMap<String, WcCommandType> wcCommandTypes;
	private FileSystem fileSystem;

	WcCommandFactory(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
		wcCommandTypes = new HashMap<>();
		wcCommandTypes.put("-l", new LineCountWcCommand(fileSystem));
	}

	public WcCommandType getWcCommand(String information) {
		return wcCommandTypes.getOrDefault(information, new DefaultWcCommand(fileSystem));
	}
}
