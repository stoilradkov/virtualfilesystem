package controller.commandparser;

import exceptions.InvalidCommandException;
import exceptions.NoSuchVirtualFileException;
import model.File;
import model.FileSystem;

abstract class WriteCommandType {
	abstract void executeWriteCommand(String information) throws InvalidCommandException, NoSuchVirtualFileException;

	protected boolean correctInput(String information) {
		return (information != null && !information.equals(""));
	}

	protected void writeToFile(FileSystem fileSystem, String information, int startIndex, int expectedPartsLength)
			throws InvalidCommandException, NoSuchVirtualFileException {
		String[] parts = information.split(" ");
		if (parts.length < expectedPartsLength) {
			throw new InvalidCommandException("The syntax of the command is invalid");
		}
		File atLocation;
		atLocation = fileSystem.findFile(parts[startIndex - 1]);
		if (atLocation == null || atLocation.isRemoved()) {
			throw new NoSuchVirtualFileException("The specified virtual file does not exist");
		}
		int lineNumber = Integer.parseInt(parts[startIndex]);
		if (lineNumber >= 0) {
			StringBuilder content = new StringBuilder("");
			for (int i = 0; i < lineNumber - 1; i++) {
				content.append("\r\n");
			}
			for (int i = startIndex + 1; i < parts.length - 1; i++) {
				content.append(parts[i] + " ");
			}
			content.append(parts[parts.length - 1]);
			atLocation.setContent(content.toString(), startIndex % 2 == 0);
			atLocation.setFileSize(atLocation.getFileSize() - lineNumber);
		} else {
			System.out.println("Negative line number");
		}
	}
}
