package controller.commandparser;

import model.FileSystem;
import model.Folder;

class DefaultCdCommand extends CdCommandType {
	private FileSystem fileSystem;

	DefaultCdCommand(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
	}

	void executeCdCommand(String location) {
		if (correctInput(location)) {
			if (location.equals("/")) {
				fileSystem.backCurrentFolderToRoot();
				return;
			}
			String[] subFolders = location.split("/");
			StringBuilder newLocation;
			Folder atLocation;
			int numberOfFolders = subFolders.length;
			if (numberOfFolders == 0) {
				return;
			}
			if (subFolders[0].equals("..")) {
				fileSystem.backCurrentFolderOnce();
				return;
			}
			int startIndex;
			if (location.charAt(0) == '/') {
				startIndex = 1;
				atLocation = fileSystem.backToRoot();
				newLocation = new StringBuilder("");
			} else {
				startIndex = 0;
				atLocation = fileSystem.currentFolder;
				if (fileSystem.currentLocation.toString().equals("/")) {
					newLocation = new StringBuilder("");
				} else {
					newLocation = new StringBuilder(fileSystem.currentLocation);
				}
			}
			boolean legalLocation = false;
			do {

				legalLocation = false;
				for (Folder iter : atLocation.folderChildren) {
					if (iter.getName().equals(subFolders[startIndex])) {
						atLocation = iter;
						newLocation.append("/" + subFolders[startIndex]);
						legalLocation = true;
						break;
					}
				}
				startIndex++;
			} while (startIndex < numberOfFolders && legalLocation);
			if (startIndex == numberOfFolders && legalLocation) {
				fileSystem.currentFolder = atLocation;
				fileSystem.currentLocation = newLocation;
			} else {
				System.out.println("The system cannot find the path specified");
			}
		}
	}
}
