package controller.commandparser;

abstract class WcCommandType {
	abstract int executeWcCommand(String information);

	protected boolean correctInput(String information) {
		return (information != null && !information.equals(""));
	}

}
