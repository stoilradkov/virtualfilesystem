package controller.commandparser;

import java.util.ArrayList;

import model.File;
import model.FileSystem;

class DescendingSortLsCommand extends LsCommandType {
	private FileSystem fileSystem;

	public DescendingSortLsCommand(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
	}

	void executeLsCommand(String command) {
		ArrayList<File> sortChildren = new ArrayList<>(fileSystem.currentFolder.fileChildren);
		int length = sortChildren.size();
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < length - i - 1; j++)
				if (!sortChildren.get(j).isRemoved() && !sortChildren.get(j + 1).isRemoved()
						&& sortChildren.get(j).getFileSize() < sortChildren.get(j + 1).getFileSize()) {
					File temp = sortChildren.get(j);
					sortChildren.set(j, sortChildren.get(j + 1));
					sortChildren.set(j + 1, temp);
				}
		}
		for (int k = 0; k < length; k++) {
			if (!sortChildren.get(k).isRemoved()) {
				System.out.println(sortChildren.get(k).getName());
			}
		}
	}
}
