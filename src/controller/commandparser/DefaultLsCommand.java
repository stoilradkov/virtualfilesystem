package controller.commandparser;

import model.File;
import model.FileSystem;

class DefaultLsCommand extends LsCommandType {
	private FileSystem fileSystem;

	public DefaultLsCommand(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
	}

	void executeLsCommand(String command) {
		for (File file : fileSystem.currentFolder.fileChildren) {
			if (!file.isRemoved()) {
				System.out.println(file.getName());
			}
		}
	}
}
