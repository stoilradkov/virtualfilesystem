package controller.commandparser;

import model.FileSystem;
import model.Folder;

class DefaultMkdirCommand extends MkdirCommandType {
	private FileSystem fileSystem;

	public DefaultMkdirCommand(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
	}
	private boolean validDirName(String dirName) {
		return !dirName.equals("\\") && !dirName.equals("/") && !dirName.equals("<")
				&& !dirName.equals(">") && !dirName.equals("*") && !dirName.equals("?")
				&& !dirName.equals(":") && !dirName.equals("\"") && !dirName.equals("|");
	}
	void executeMkdirCommand(String location) {
		if (correctInput(location)) {
			String[] subFolders = location.split("/");
			Folder atLocation;
			int numberOfFolders = subFolders.length;
			if (numberOfFolders == 0) {
				return;
			}
			int startIndex;
			if (location.charAt(0) == '/') {
				startIndex = 1;
				atLocation = fileSystem.backToRoot();
			} else {
				startIndex = 0;
				atLocation = fileSystem.currentFolder;
			}
			if(!validDirName(subFolders[startIndex])) {
				return;
			}
			boolean exists;
			do {
				exists = false;
				for (Folder iter : atLocation.folderChildren) {
					if (iter.getName().equals(subFolders[startIndex])) {
						atLocation = iter;
						exists = true;
						break;
					}
				}
				if (!exists) {
					atLocation.addFolderChild(new Folder(subFolders[startIndex]));
					atLocation = atLocation.folderChildren.get(atLocation.folderChildren.size() - 1);
					System.out.println("Created new folder!");
					fileSystem.numberOfNodes++;
				}
				startIndex++;
			} while (startIndex < numberOfFolders);
		}
	}
}
