package controller.commandparser;

import exceptions.NoSuchVirtualFileException;
import model.FileSystem;

class CatCommand extends Command {
	public CatCommand(FileSystem fileSystem) {
		super(fileSystem);
	}

	@Override
	public int executeCommand(String information) {
		if (correctInput(information)) {
			CatCommandFactory catCommandFactory = new CatCommandFactory(this.fileSystem);
			StringBuilder option = new StringBuilder();
			int i = 0;
			while (i < information.length() && information.charAt(i) != ' ') {
				option.append(information.charAt(i));
				i++;
			}
			CatCommandType catType = catCommandFactory.getCatCommand(option.toString());
			try {
				catType.executeCatCommand(information);
			} catch (NoSuchVirtualFileException e) {
				e.printStackTrace();
			}
			return 1;
		}
		System.out.println("The syntax of the command is incorrect");
		return 0;
	}

	@Override
	public boolean correctInput(String information) {
		return information != null;
	}
}
