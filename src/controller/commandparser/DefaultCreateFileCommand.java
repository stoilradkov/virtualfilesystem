package controller.commandparser;

import exceptions.DuplicateVirtualFileNameException;
import model.File;
import model.FileSystem;

class DefaultCreateFileCommand extends CreateFileCommandType {
	private FileSystem fileSystem;

	public DefaultCreateFileCommand(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
	}

	protected boolean correctInput(String fileName) {
		return fileName != null && !fileName.equals("") && fileName.matches(".*txt");
	}

	void executeCreateFileCommand(String fileName) throws DuplicateVirtualFileNameException {
		if (correctInput(fileName)) {
			for (File iter : fileSystem.currentFolder.fileChildren) {
				if (iter.getName().equals(fileName) && !iter.isRemoved()) {
					throw new DuplicateVirtualFileNameException("File already exists");
				}
			}
			boolean created = false;
			for (File file : fileSystem.currentFolder.fileChildren) {
				if (file.isRemoved()) {
					file.setName(fileName);
					file.setContent("", true);
					file.setRemoved(false);
					created = true;
					break;
				}
			}
			if (!created) {
				fileSystem.currentFolder.addFileChild(new File(fileName));
			}
			System.out.println("Created new file!");
			fileSystem.numberOfNodes++;
			return;
		}
		System.out.println("Incorrect file name");
	}
}
