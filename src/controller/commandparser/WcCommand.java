package controller.commandparser;

import model.FileSystem;

class WcCommand extends Command {
	public WcCommand(FileSystem fileSystem) {
		super(fileSystem);
	}

	@Override
	public int executeCommand(String information) {
		if (correctInput(information)) {
			WcCommandFactory wcCommandFactory = new WcCommandFactory(this.fileSystem);
			StringBuilder option = new StringBuilder();
			int i = 0;
			while (i < information.length() && information.charAt(i) != ' ') {
				option.append(information.charAt(i));
				i++;
			}
			WcCommandType wcType = wcCommandFactory.getWcCommand(option.toString());
			wcType.executeWcCommand(information);
			return 1;
		}
		System.out.println("The syntax of the command is incorrect");
		return 0;
	}

	@Override
	public boolean correctInput(String information) {
		return information != null;
	}

}
