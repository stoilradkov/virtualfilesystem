package controller.commandparser;

import exceptions.InvalidCommandException;
import exceptions.NoSuchVirtualFileException;
import model.FileSystem;

class RemoveCommand extends Command {
	public RemoveCommand(FileSystem fileSystem) {
		super(fileSystem);
	}

	@Override
	public int executeCommand(String information) {
		if (correctInput(information)) {
			RemoveCommandFactory removeCommandFactory = new RemoveCommandFactory(this.fileSystem);
			StringBuilder option = new StringBuilder();
			int i = 0;
			while (i < information.length() && information.charAt(i) != ' ') {
				option.append(information.charAt(i));
				i++;
			}
			RemoveCommandType removeType = removeCommandFactory.getRemoveCommand(option.toString());
			try {
				removeType.executeRemoveCommand(information);
			} catch (InvalidCommandException | NoSuchVirtualFileException | NumberFormatException e) {
				e.printStackTrace();
			}
			return 1;
		}
		return 0;
	}

	@Override
	public boolean correctInput(String information) {
		return information != null;
	}
}
