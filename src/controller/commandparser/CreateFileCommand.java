package controller.commandparser;

import exceptions.DuplicateVirtualFileNameException;
import model.FileSystem;

class CreateFileCommand extends Command {
	public CreateFileCommand(FileSystem fileSystem) {
		super(fileSystem);
	}

	@Override
	public int executeCommand(String fileName) {
		if (correctInput(fileName)) {
			CreateFileCommandFactory createFileCommandFactory = new CreateFileCommandFactory(this.fileSystem);
			StringBuilder option = new StringBuilder();
			int i = 0;
			while (i < fileName.length() && fileName.charAt(i) != ' ') {
				option.append(fileName.charAt(i));
				i++;
			}
			CreateFileCommandType createFileType = createFileCommandFactory.getCreateFileCommand(option.toString());
			try {
				createFileType.executeCreateFileCommand(fileName);
			} catch (DuplicateVirtualFileNameException e) {
				e.printStackTrace();
			}
			return 1;
		}
		System.out.println("The syntax of the command is incorrect");
		return 0;
	}

	@Override
	public boolean correctInput(String fileName) {
		return fileName != null;
	}
}
