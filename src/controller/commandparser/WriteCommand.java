package controller.commandparser;

import exceptions.InvalidCommandException;
import exceptions.NoSuchVirtualFileException;
import model.FileSystem;

class WriteCommand extends Command {
	public WriteCommand(FileSystem fileSystem) {
		super(fileSystem);
	}

	@Override
	public int executeCommand(String information) {
		if (correctInput(information)) {
			WriteCommandFactory writeCommandFactory = new WriteCommandFactory(this.fileSystem);
			StringBuilder option = new StringBuilder();
			int i = 0;
			while (i < information.length() && information.charAt(i) != ' ') {
				option.append(information.charAt(i));
				i++;
			}
			WriteCommandType writeType = writeCommandFactory.getWriteCommand(option.toString());
			try {
				writeType.executeWriteCommand(information);
			} catch (InvalidCommandException | NoSuchVirtualFileException | NumberFormatException e) {
				e.printStackTrace();
			}
			return 1;
		}
		System.out.println("The syntax of the command is incorrect");
		return 0;
	}

	@Override
	public boolean correctInput(String information) {
		return information != null;
	}

}
