package controller.commandparser;

import java.util.HashMap;

import model.FileSystem;

class CreateFileCommandFactory {
	private HashMap<String, CreateFileCommandType> createFileCommandTypes;
	private FileSystem fileSystem;

	CreateFileCommandFactory(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
		createFileCommandTypes = new HashMap<>();
	}

	public CreateFileCommandType getCreateFileCommand(String information) {
		return createFileCommandTypes.getOrDefault(information, new DefaultCreateFileCommand(fileSystem));
	}
}
