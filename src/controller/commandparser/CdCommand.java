package controller.commandparser;

import model.FileSystem;

class CdCommand extends Command {
	public CdCommand(FileSystem fileSystem) {
		super(fileSystem);
	}

	@Override
	public int executeCommand(String information) {
		if (correctInput(information)) {
			CdCommandFactory cdCommandFactory = new CdCommandFactory(this.fileSystem);
			StringBuilder option = new StringBuilder();
			int i = 0;
			while (i < information.length() && information.charAt(i) != ' ') {
				option.append(information.charAt(i));
				i++;
			}
			CdCommandType cdType = cdCommandFactory.getCdCommand(option.toString());
			cdType.executeCdCommand(information);
			return 1;
		}
		System.out.println("The syntax of the command is incorrect");
		return 0;
	}

	@Override
	public boolean correctInput(String information) {
		return information != null;
	}
}
