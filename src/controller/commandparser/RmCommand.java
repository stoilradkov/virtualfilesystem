package controller.commandparser;

import exceptions.NoSuchVirtualFileException;
import model.FileSystem;

class RmCommand extends Command {
	public RmCommand(FileSystem fileSystem) {
		super(fileSystem);
	}

	@Override
	public int executeCommand(String information) {
		if (correctInput(information)) {
			RmCommandFactory rmCommandFactory = new RmCommandFactory(this.fileSystem);
			StringBuilder option = new StringBuilder();
			int i = 0;
			while (i < information.length() && information.charAt(i) != ' ') {
				option.append(information.charAt(i));
				i++;
			}
			RmCommandType rmType = rmCommandFactory.getRmCommand(option.toString());
			try {
				rmType.executeRmCommand(information);
			} catch (NoSuchVirtualFileException e) {
				e.printStackTrace();
			}
			return 1;
		}
		System.out.println("The sytax of the command is incorrect");
		return 0;
	}

	@Override
	public boolean correctInput(String information) {
		return information != null;
	}
}
