package controller.commandparser;

import exceptions.NoSuchVirtualFileException;
import model.File;
import model.FileSystem;

class DefaultRmCommand extends RmCommandType {
	private FileSystem fileSystem;

	public DefaultRmCommand(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
	}

	void executeRmCommand(String fileName) throws NoSuchVirtualFileException {
		if (correctInput(fileName)) {
			File atLocation = fileSystem.findFile(fileName);
			if (atLocation == null || atLocation.isRemoved()) {
				throw new NoSuchVirtualFileException("The specified virtual file does not exist");
			}
			atLocation.setRemoved(true);
			System.out.println("Removed file");
		}
	}
}
