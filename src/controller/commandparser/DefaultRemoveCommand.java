package controller.commandparser;

import exceptions.InvalidCommandException;
import exceptions.NoSuchVirtualFileException;
import model.File;
import model.FileSystem;

public class DefaultRemoveCommand extends RemoveCommandType {
	private FileSystem fileSystem;

	public DefaultRemoveCommand(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
	}

	@Override
	void executeRemoveCommand(String information) throws InvalidCommandException, NoSuchVirtualFileException {
		if (correctInput(information)) {
			String[] parts = information.split(" ");
			if (parts.length != 2) {
				throw new InvalidCommandException("The syntax of the command is incorrect");
			}
			File atLocation;
			atLocation = fileSystem.findFile(parts[0]);
			if (atLocation == null || atLocation.isRemoved()) {
				throw new NoSuchVirtualFileException("The specified virtual file does not exist");
			}
			String[] lines = parts[1].split("-");
			if (lines.length != 2) {
				throw new InvalidCommandException("The syntax of the command is incorrect");
			}
			int fromLine = Integer.parseInt(lines[0]);
			int toLine = Integer.parseInt(lines[1]);
			if (toLine <= fromLine || toLine < 0 || fromLine < 0) {
				throw new InvalidCommandException("Illegal arguments");
			}
			StringBuilder content = new StringBuilder(atLocation.getContent());
			String[] linesInFile = content.toString().split("\\r?\\n");
			if ((toLine - fromLine) > linesInFile.length || fromLine >= linesInFile.length) {
				throw new InvalidCommandException("Illegal arguments");
			}
			int replaceFrom = 0;
			int linesToRemove = 0;
			int index = 0;
			while (linesToRemove != fromLine) {
				while (content.toString().charAt(index) != '\r') {
					replaceFrom++;
					index++;
				}
				linesToRemove++;
				replaceFrom += 2;
				index += 2;
			}
			if (toLine < linesInFile.length - 1) {
				content.replace(replaceFrom, content.length(), linesInFile[toLine] + "\r\n");
			} else if (toLine < linesInFile.length) {
				content.replace(replaceFrom, content.length(), linesInFile[toLine]);
			} else {
				if (fromLine == 0) {
					content.replace(replaceFrom, content.length(), "");
				} else {
					content.replace(replaceFrom - 2, content.length(), "");
				}
			}
			for (int i = toLine + 1; i < linesInFile.length; i++) {
				content.append(linesInFile[i]);
			}
			atLocation.setContent(content.toString(), true);
			return;
		}
		System.out.println("The syntax of the command is incorrect");
		return;
	}
}
