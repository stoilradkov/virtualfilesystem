package controller.commandparser;

import exceptions.InvalidCommandException;
import exceptions.NoSuchVirtualFileException;

abstract class RemoveCommandType {
	abstract void executeRemoveCommand(String information) throws InvalidCommandException, NoSuchVirtualFileException;

	protected boolean correctInput(String information) {
		return (information != null && !information.equals(""));
	}

}
