package controller.commandparser;

import java.util.HashMap;

import model.FileSystem;

class CommandFactory {
	private HashMap<String, Command> allCommands;
	private FileSystem fileSystem;

	public CommandFactory() {
		fileSystem = new FileSystem();
		allCommands = new HashMap<>();
		allCommands.put("cd", new CdCommand(fileSystem));
		allCommands.put("mkdir", new MkdirCommand(fileSystem));
		allCommands.put("write", new WriteCommand(fileSystem));
		allCommands.put("create_file", new CreateFileCommand(fileSystem));
		allCommands.put("cat", new CatCommand(fileSystem));
		allCommands.put("ls", new LsCommand(fileSystem));
		allCommands.put("wc", new WcCommand(fileSystem));
		allCommands.put("rm", new RmCommand(fileSystem));
		allCommands.put("remove", new RemoveCommand(fileSystem));
	}

	public Command getCommand(String command) {
		return allCommands.get(command);
	}
}
