package controller.commandparser;

import exceptions.InvalidCommandException;
import exceptions.NoSuchVirtualFileException;
import model.FileSystem;

class OverwriteWriteCommand extends WriteCommandType {
	private FileSystem fileSystem;

	public OverwriteWriteCommand(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
	}

	void executeWriteCommand(String information) throws InvalidCommandException, NoSuchVirtualFileException {
		if (correctInput(information)) {
			writeToFile(fileSystem, information, 2, 4);

		}
	}
}
