package controller.commandparser;

import java.util.Scanner;

import exceptions.InvalidCommandException;

public class InputParser {
	private Scanner scanInput;

	public InputParser(Scanner scanInput) {
		this.scanInput = scanInput;
	}

	public void parseUserInput() throws InvalidCommandException {
		StringBuilder input = new StringBuilder("");
		CommandFactory commandParser = new CommandFactory();
		System.out.print("/home ");
		do {
			input.replace(0, input.length(), scanInput.nextLine()); // TODO check if pipeline
			if (input.toString().equals("exit")) {
				return;
			}
			StringBuilder commandType = new StringBuilder("");
			int i = 0;
			while (i < input.length() && input.charAt(i) != ' ') {
				commandType.append(input.charAt(i));
				i++;
			}
			Command command = commandParser.getCommand(commandType.toString());
			if (command == null) {
				throw new InvalidCommandException("Unrecognized command");
			}
			if (i + 1 < input.length()) {
				command.executeCommand(input.substring(i + 1));
			} else {
				command.executeCommand(null);
			}
			System.out.print(command.getCurrentLocation() + " ");
		} while (true);
	}
}
