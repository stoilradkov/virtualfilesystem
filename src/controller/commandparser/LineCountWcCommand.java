package controller.commandparser;

import model.File;
import model.FileSystem;

class LineCountWcCommand extends WcCommandType {
	private FileSystem fileSystem;

	public LineCountWcCommand(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
	}

	private String getContent(String content) {
		if (content.matches(".*txt")) {
			File atLocation = fileSystem.findFile(content);
			if (atLocation == null || atLocation.isRemoved()) {
				return content;
			}
			return atLocation.getContent();
		} else {
			return content;
		}
	}

	public int executeWcCommand(String information) {
		String content;
		if (information.length() < 4) {
			return 0;
		}
		content = getContent(information.substring(3));
		String[] lines = content.split("\\r?\\n");
		System.out.println(lines.length);
		return lines.length;

	}
}
