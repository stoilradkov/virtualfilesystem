package controller.commandparser;

import exceptions.NoSuchVirtualFileException;

abstract class CatCommandType {
	abstract void executeCatCommand(String information) throws NoSuchVirtualFileException;

	protected boolean correctInput(String information) {
		return (information != null && !information.equals(""));
	}
}
