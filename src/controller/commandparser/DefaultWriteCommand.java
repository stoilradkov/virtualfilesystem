package controller.commandparser;

import exceptions.InvalidCommandException;
import exceptions.NoSuchVirtualFileException;
import model.FileSystem;

class DefaultWriteCommand extends WriteCommandType {
	private FileSystem fileSystem;

	public DefaultWriteCommand(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
	}

	void executeWriteCommand(String information) throws InvalidCommandException, NoSuchVirtualFileException {
		if (correctInput(information)) {
			writeToFile(fileSystem, information, 1, 3);
			return;
		}
		System.out.println("The syntax of the command is incorrect");
	}
}