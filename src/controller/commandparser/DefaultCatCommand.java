package controller.commandparser;

import exceptions.NoSuchVirtualFileException;
import model.File;
import model.FileSystem;

class DefaultCatCommand extends CatCommandType {
	private FileSystem fileSystem;

	DefaultCatCommand(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
	}

	void executeCatCommand(String fileName) throws NoSuchVirtualFileException {
		if (correctInput(fileName)) {
			File atLocation = fileSystem.findFile(fileName);
			if (atLocation == null || atLocation.isRemoved()) {
				throw new NoSuchVirtualFileException("The specified virtual file does not exist");
			}
			System.out.println(atLocation.getContent());
		}
	}
}
