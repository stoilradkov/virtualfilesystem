package controller.commandparser;

import java.util.HashMap;

import model.FileSystem;

class LsCommandFactory {
	private HashMap<String, LsCommandType> lsCommandTypes;
	private FileSystem fileSystem;

	LsCommandFactory(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
		lsCommandTypes = new HashMap<>();
		lsCommandTypes.put("--sorted desc", new DescendingSortLsCommand(fileSystem));
	}

	public LsCommandType getLsCommand(String information) {
		return lsCommandTypes.getOrDefault(information, new DefaultLsCommand(fileSystem));
	}
}
