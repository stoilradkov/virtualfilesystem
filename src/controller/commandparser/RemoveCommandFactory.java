package controller.commandparser;

import java.util.HashMap;

import model.FileSystem;

class RemoveCommandFactory {
	private HashMap<String, RemoveCommandType> removeCommandTypes;
	private FileSystem fileSystem;

	RemoveCommandFactory(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
		removeCommandTypes = new HashMap<>();
	}

	public RemoveCommandType getRemoveCommand(String information) {
		return removeCommandTypes.getOrDefault(information, new DefaultRemoveCommand(fileSystem));
	}
}
