package controller.commandparser;

import java.util.HashMap;

import model.FileSystem;

class RmCommandFactory {
	private HashMap<String, RmCommandType> rmCommandTypes;
	private FileSystem fileSystem;

	RmCommandFactory(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
		rmCommandTypes = new HashMap<>();
	}

	public RmCommandType getRmCommand(String information) {
		return rmCommandTypes.getOrDefault(information, new DefaultRmCommand(fileSystem));
	}
}
