package controller.commandparser;

import java.util.HashMap;

import model.FileSystem;

class WriteCommandFactory {
	private HashMap<String, WriteCommandType> writeCommandTypes;
	private FileSystem fileSystem;

	WriteCommandFactory(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
		writeCommandTypes = new HashMap<>();
		writeCommandTypes.put("-overwrite", new OverwriteWriteCommand(fileSystem));
	}

	public WriteCommandType getWriteCommand(String information) {
		return writeCommandTypes.getOrDefault(information, new DefaultWriteCommand(fileSystem));
	}
}