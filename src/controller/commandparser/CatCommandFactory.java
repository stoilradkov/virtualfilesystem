package controller.commandparser;

import java.util.HashMap;

import model.FileSystem;

class CatCommandFactory {
	private HashMap<String, CatCommandType> catCommandTypes;
	private FileSystem fileSystem;

	CatCommandFactory(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
		catCommandTypes = new HashMap<>();
	}

	public CatCommandType getCatCommand(String information) {
		return catCommandTypes.getOrDefault(information, new DefaultCatCommand(fileSystem));
	}
}
