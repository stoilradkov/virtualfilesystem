package controller.commandparser;

abstract class CdCommandType {
	abstract void executeCdCommand(String information);

	protected boolean correctInput(String information) {
		return (information != null && !information.equals(""));
	}

}
