package controller.commandparser;

abstract class MkdirCommandType {
	abstract void executeMkdirCommand(String information);

	protected boolean correctInput(String information) {
		return (information != null && !information.equals(""));
	}

}