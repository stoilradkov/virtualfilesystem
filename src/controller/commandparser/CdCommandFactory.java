package controller.commandparser;

import java.util.HashMap;

import model.FileSystem;

class CdCommandFactory {
	private HashMap<String, CdCommandType> cdCommandTypes;
	private FileSystem fileSystem;

	CdCommandFactory(FileSystem fileSystem) {
		this.fileSystem = fileSystem;
		cdCommandTypes = new HashMap<>();
	}

	public CdCommandType getCdCommand(String information) {
		return cdCommandTypes.getOrDefault(information, new DefaultCdCommand(fileSystem));
	}
}
