package model;

import java.util.ArrayList;

public class Folder {
	private String name;
	public ArrayList<File> fileChildren = new ArrayList<File>();
	public ArrayList<Folder> folderChildren = new ArrayList<Folder>();
	Folder parent = null;

	public Folder(String name) {
		this.name = name;
	}

	public void addFolderChild(Folder child) {
		child.setParent(this);
		this.folderChildren.add(child);
	}

	public void addFileChild(File child) {
		child.setParent(this);
		this.fileChildren.add(child);
	}

	public String getName() {
		return name;
	}

	protected void setParent(Folder parent) {
		this.parent = parent;
	}

	public String toString() {
		return name;
	}
}
