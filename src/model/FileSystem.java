package model;

public class FileSystem {
	public Folder currentFolder;
	public File currentFile;
	public StringBuilder currentLocation;
	public int numberOfNodes;

	public FileSystem() {
		this.currentFolder = new Folder("/");
		this.currentFolder.addFolderChild(new Folder("home"));
		this.currentFolder = this.currentFolder.folderChildren.get(0);
		currentLocation = new StringBuilder("/home");
		this.numberOfNodes = 2;
	}

	public Folder backToRoot() {
		Folder result = currentFolder;
		while (result.parent != null) {
			result = result.parent;
		}
		return result;
	}

	public void backCurrentFolderOnce() {
		if (currentFolder.parent != null) {
			currentFolder = currentFolder.parent;
			int i = currentLocation.length() - 1;
			char c;
			while ((c = currentLocation.charAt(i)) != '/') {
				i--;
			}
			if (currentFolder.parent != null) {
				currentLocation.delete(i, currentLocation.length());
			} else {
				currentLocation.delete(i + 1, currentLocation.length());
			}
		}
	}

	public void backCurrentFolderToRoot() {
		while (currentFolder.parent != null) {
			backCurrentFolderOnce();
		}
	}

	public File findFile(String fileName) {
		for (File child : currentFolder.fileChildren) {
			if (child.getName().equals(fileName)) {
				return child;
			}
		}
		return null;
	}

	public int getNumberOfNodes() {
		return this.numberOfNodes;
	}

	public String getCurrentLocation() {
		return this.currentLocation.toString();
	}

	public Folder getCurrentFolder() {
		return currentFolder;
	}

	public File getCurrentFile() {
		return currentFile;
	}

	public Folder getFolderParent() {
		if (currentFolder.parent != null) {
			return currentFolder.parent;
		} else {
			return null;
		}
	}

	public Folder getFileParent() {
		if (currentFile.parent != null) {
			return currentFile.parent;
		} else {
			return null;
		}
	}
}
