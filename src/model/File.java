package model;

public class File {
	private StringBuilder name;
	Folder parent = null;
	private StringBuilder content;
	private int fileSize;
	private boolean removed;

	public File(String name) {
		this.name = new StringBuilder(name);
		this.fileSize = 0;
		this.content = new StringBuilder("");
		this.removed = false;
	}

	public void setContent(String content, boolean overwrite) {
		if (overwrite) {
			this.content.replace(0, this.content.length(), content);
			this.fileSize = content.length();
		} else {
			this.content.append(content);
			this.fileSize += content.length();
		}
	}

	public String getName() {
		return name.toString();
	}

	public void setName(String newName) {
		this.name.replace(0, name.length(), newName);
	}

	protected void setParent(Folder parent) {
		this.parent = parent;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public int getFileSize() {
		return this.fileSize;
	}

	public String getContent() {
		return content.toString();
	}

	public boolean isRemoved() {
		return removed;
	}

	public void setRemoved(boolean removed) {
		this.removed = removed;
	}
}
