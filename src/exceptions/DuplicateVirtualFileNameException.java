package exceptions;

public class DuplicateVirtualFileNameException extends Exception {
	private static final long serialVersionUID = -6949925947865684830L;

	public DuplicateVirtualFileNameException() {
		super();
	}

	public DuplicateVirtualFileNameException(String message) {
		super(message);
	}

	public DuplicateVirtualFileNameException(String message, Throwable cause) {
		super(message, cause);
	}

	public DuplicateVirtualFileNameException(Throwable cause) {
		super(cause);
	}

}
