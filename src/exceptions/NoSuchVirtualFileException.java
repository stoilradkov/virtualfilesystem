package exceptions;

public class NoSuchVirtualFileException extends Exception {
	private static final long serialVersionUID = -3106093635870252947L;

	public NoSuchVirtualFileException() {
		super();
	}

	public NoSuchVirtualFileException(String message) {
		super(message);
	}

	public NoSuchVirtualFileException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoSuchVirtualFileException(Throwable cause) {
		super(cause);
	}
}
