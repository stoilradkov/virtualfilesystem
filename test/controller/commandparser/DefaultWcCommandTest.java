package controller.commandparser;

import static org.junit.Assert.*;

import org.junit.Test;

import exceptions.NoSuchVirtualFileException;
import model.File;
import model.FileSystem;

public class DefaultWcCommandTest {

	@Test
	public void givenDefaultWcCommandWithNoFilesWhenExecuteWcCommandInvokedOnTextThenReturnNumberOfWordsInText() {
		FileSystem fileSystem = new FileSystem();
		DefaultWcCommand wcCommand = new DefaultWcCommand(fileSystem);
		int result = wcCommand.executeWcCommand("one two three");
		assertEquals(3, result);
	}

	@Test
	public void givenDefaultWcCommandWithFilesWhenExecuteWcCommandInvokedOnFileNameThenReturnNumberOfWordsInFile() {
		FileSystem fileSystem = new FileSystem();
		DefaultWcCommand wcCommand = new DefaultWcCommand(fileSystem);
		File file = new File("file.txt");
		fileSystem.currentFolder.fileChildren.add(file);
		file.setContent("one two ", true);
		int result = wcCommand.executeWcCommand("file.txt");
		assertEquals(2, result);
	}

	@Test
	public void givenDefaultWcCommandWithFilesWhenExecuteWcCommandInvokedWithDeletedFileNameThenReturnNumberOfWordsInText()
			throws NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultWcCommand wcCommand = new DefaultWcCommand(fileSystem);
		File file = new File("file.txt");
		fileSystem.currentFolder.fileChildren.add(file);
		DefaultRmCommand rmCommand = new DefaultRmCommand(fileSystem);
		rmCommand.executeRmCommand("file.txt");
		int result = wcCommand.executeWcCommand("file.txt");
		assertEquals(1, result);
	}

}
