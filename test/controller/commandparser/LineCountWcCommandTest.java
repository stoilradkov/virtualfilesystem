package controller.commandparser;

import static org.junit.Assert.*;

import org.junit.Test;

import model.File;
import model.FileSystem;

public class LineCountWcCommandTest {

	@Test
	public void givenLineCountWcCommandWithNoFilesWhenExecuteWcCommandInvokedOnTextThenReturnNumberOfLinesInText() {
		FileSystem fileSystem = new FileSystem();
		LineCountWcCommand wcCommand = new LineCountWcCommand(fileSystem);
		int result = wcCommand.executeWcCommand("-l one two three\r\nsecond line");
		assertEquals(2, result);
	}

	@Test
	public void givenLineCountWcCommandWithFilesWhenExecuteWcCommandInvokedOnFileNameThenReturnNumberOfLinesInFile() {
		FileSystem fileSystem = new FileSystem();
		LineCountWcCommand wcCommand = new LineCountWcCommand(fileSystem);
		File file = new File("file.txt");
		fileSystem.currentFolder.fileChildren.add(file);
		file.setContent("one two\r\n three", true);
		int result = wcCommand.executeWcCommand("-l file.txt");
		assertEquals(2, result);
	}

}
