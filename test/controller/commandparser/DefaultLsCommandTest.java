package controller.commandparser;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;
import org.mockito.Mockito;

import model.File;
import model.FileSystem;

public class DefaultLsCommandTest {

	@Test
	public void givenDefaultLsCommandWithNoFilesWhenDefaultLsCommandInvokedThenPrintEmptyString() {
		FileSystem fileSystem = new FileSystem();
		DefaultLsCommand lsCommand = new DefaultLsCommand(fileSystem);
		ByteArrayOutputStream contentToBePrinted = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(contentToBePrinted);
		PrintStream old = System.out;
		System.setOut(ps);
		lsCommand.executeLsCommand(null);
		System.out.flush();
		assertEquals("", contentToBePrinted.toString());
		System.setOut(old);
	}

	@Test
	public void givenDefaultLsCommandWithFilesWhenLsIvokedThenPrintFileNames() {
		FileSystem fileSystem = new FileSystem();
		DefaultLsCommand lsCommand = new DefaultLsCommand(fileSystem);
		File file1 = Mockito.mock(File.class);
		File file2 = Mockito.mock(File.class);
		fileSystem.currentFolder.fileChildren.add(file1);
		fileSystem.currentFolder.fileChildren.add(file2);
		Mockito.when(file1.getName()).thenReturn("file1.txt");
		Mockito.when(file2.getName()).thenReturn("file2.txt");
		ByteArrayOutputStream contentToBePrinted = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(contentToBePrinted);
		PrintStream old = System.out;
		System.setOut(ps);
		lsCommand.executeLsCommand("");
		System.out.flush();
		assertEquals("file1.txt\r\nfile2.txt", contentToBePrinted.toString().trim());
		System.setOut(old);
	}

}
