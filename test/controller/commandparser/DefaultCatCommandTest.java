package controller.commandparser;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;
import org.mockito.Mockito;

import exceptions.NoSuchVirtualFileException;
import model.File;
import model.FileSystem;

public class DefaultCatCommandTest {

	@Test
	public void givenDefaultCatCommandWithEmptyFileWhenExecuteCatCommandInvokedWithExistingFileThenPrintEmptyString()
			throws NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultCatCommand catCommand = new DefaultCatCommand(fileSystem);
		File file = Mockito.mock(File.class);
		Mockito.when(file.getName()).thenReturn("file.txt");
		Mockito.when(file.getContent()).thenReturn("");
		fileSystem.currentFolder.fileChildren.add(file);
		ByteArrayOutputStream contentToBePrinted = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(contentToBePrinted);
		PrintStream old = System.out;
		System.setOut(ps);
		catCommand.executeCatCommand("file.txt");
		System.out.flush();
		assertEquals("", contentToBePrinted.toString().trim());
		System.setOut(old);
	}

	@Test
	public void givenDefaultCatCommandWithNonEmptyFileWhenExecuteCatCommandInvokedWithThisFileThenPrintFileContent()
			throws NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultCatCommand catCommand = new DefaultCatCommand(fileSystem);
		File file = Mockito.mock(File.class);
		Mockito.when(file.getName()).thenReturn("file.txt");
		Mockito.when(file.getContent()).thenReturn("file content");
		fileSystem.currentFolder.fileChildren.add(file);
		ByteArrayOutputStream contentToBePrinted = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(contentToBePrinted);
		PrintStream old = System.out;
		System.setOut(ps);
		catCommand.executeCatCommand("file.txt");
		System.out.flush();
		assertEquals("file content".trim(), contentToBePrinted.toString().trim());
		System.setOut(old);
	}

	@Test(expected = NoSuchVirtualFileException.class)
	public void givenDefaultCatCommandWithFileWhenExecuteCatCommandInvokedWithNonExistentFileNameThenThrowNoSuchVirtualFileException()
			throws NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultCatCommand catCommand = new DefaultCatCommand(fileSystem);
		File file = Mockito.mock(File.class);
		Mockito.when(file.getName()).thenReturn("file.txt");
		catCommand.executeCatCommand("file1.txt");
	}

	@Test(expected = NoSuchVirtualFileException.class)
	public void givenDefaultCatCommandWithFileWhenExecuteCatCommandInvokedWithRemovedFileNameThenThrowNoSuchVirtualFileException()
			throws NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultCatCommand catCommand = new DefaultCatCommand(fileSystem);
		File file = Mockito.mock(File.class);
		Mockito.when(file.getName()).thenReturn("file.txt");
		Mockito.when(file.isRemoved()).thenReturn(true);
		catCommand.executeCatCommand("file.txt");
	}
}
