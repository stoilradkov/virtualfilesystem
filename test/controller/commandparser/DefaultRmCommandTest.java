package controller.commandparser;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mockito;

import exceptions.NoSuchVirtualFileException;
import model.File;
import model.FileSystem;

public class DefaultRmCommandTest {

	@Test(expected = NoSuchVirtualFileException.class)
	public void givenDefaultRmCommandWithNoFilesWhenExecuteRmCommandInvokedWithFileNameThenThrowNoSuchVirtualFileException()
			throws NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultRmCommand rmCommand = new DefaultRmCommand(fileSystem);
		rmCommand.executeRmCommand("file.txt");
	}

	@Test(expected = NoSuchVirtualFileException.class)
	public void givenDefaultRmCommandWithFilesWhenExecuteRmCommandInvokedWithIncorrectFileNameThenThrowNoSuchVirtualFileException()
			throws NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		File file = Mockito.mock(File.class);
		Mockito.when(file.getName()).thenReturn("file1.txt");
		DefaultRmCommand rmCommand = new DefaultRmCommand(fileSystem);
		rmCommand.executeRmCommand("file.txt");
	}

	@Test
	public void givenDefaultRmCommandWithFilesWhenExecuteRmCommandInvokedWithExistingFileNameThenRemoveFile()
			throws NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		File file = new File("file.txt");
		fileSystem.currentFolder.fileChildren.add(file);
		DefaultRmCommand rmCommand = new DefaultRmCommand(fileSystem);
		rmCommand.executeRmCommand("file.txt");
		assertTrue(file.isRemoved());
	}

	@Test(expected = NoSuchVirtualFileException.class)
	public void givenDefaultRmCommandWithFilesWhenExecuteRmCommandInvokedWithDeletedFileNameThenThrowNoSuchVirtualFileException()
			throws NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		File file = new File("file.txt");
		fileSystem.currentFolder.fileChildren.add(file);
		DefaultRmCommand rmCommand = new DefaultRmCommand(fileSystem);
		rmCommand.executeRmCommand("file.txt");
		rmCommand.executeRmCommand("file.txt");
	}
}
