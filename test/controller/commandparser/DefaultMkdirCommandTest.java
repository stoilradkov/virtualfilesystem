package controller.commandparser;

import static org.junit.Assert.*;

import org.junit.Test;

import model.FileSystem;

public class DefaultMkdirCommandTest {
	@Test
	public void givenDefaultMkdirCommandWhenExecuteMkdirCommandInvokedWithExistingFolderThenDoNothing() {
		FileSystem fileSystem = new FileSystem();
		DefaultMkdirCommand mkdirCommand = new DefaultMkdirCommand(fileSystem);
		DefaultCdCommand cdCommand = new DefaultCdCommand(fileSystem);
		cdCommand.executeCdCommand("/");
		mkdirCommand.executeMkdirCommand("home");
		assertEquals(2, fileSystem.getNumberOfNodes());
	}

	@Test
	public void givenDefaultMkdirCommandWhenExecuteMkdirCommandInvokedWithNewFolderNameThenCreateNewFolder() {
		FileSystem fileSystem = new FileSystem();
		DefaultMkdirCommand mkdirCommand = new DefaultMkdirCommand(fileSystem);
		mkdirCommand.executeMkdirCommand("folder");
		assertEquals("folder", fileSystem.currentFolder.folderChildren.get(0).getName());
	}

	@Test
	public void givenDefaultMkdirCommandWhenExecuteMkdirCommandInvokedWithFullPathThenFolders() {
		FileSystem fileSystem = new FileSystem();
		DefaultMkdirCommand mkdirCommand = new DefaultMkdirCommand(fileSystem);
		mkdirCommand.executeMkdirCommand("folder/folder");
		assertEquals(4, fileSystem.getNumberOfNodes());
	}

}
