package controller.commandparser;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mockito;

import exceptions.DuplicateVirtualFileNameException;
import exceptions.InvalidCommandException;
import exceptions.NoSuchVirtualFileException;
import model.File;
import model.FileSystem;

public class OverwriteWriteCommandTest {

	@Test(expected = NoSuchVirtualFileException.class)
	public void givenOverwritetWriteCommandWithFileWhenOverwriteWriteCommandInvokedWithNonExistentFileNameThenThrowNoSuchVirtualFileException()
			throws InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		OverwriteWriteCommand writeCommand = new OverwriteWriteCommand(fileSystem);
		File file = Mockito.mock(File.class);
		fileSystem.currentFolder.fileChildren.add(file);
		Mockito.when(file.getName()).thenReturn("file.txt");
		writeCommand.executeWriteCommand("-overwrite file1.txt 2 hello");
	}

	@Test(expected = InvalidCommandException.class)
	public void givenOverwriteWriteCommandWhenOverwriteWriteCommandInvokedWithIncorrectSyntaxThenThrowInvalidCommandException()
			throws InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		OverwriteWriteCommand writeCommand = new OverwriteWriteCommand(fileSystem);
		writeCommand.executeWriteCommand("-overwrite file.txt 2 ");
	}

	@Test(expected = NumberFormatException.class)
	public void givenOverwriteWriteCommandWhenOverwriteWriteCommandInvokedWithWrongLineNumberThenThrowNumberFormatException()
			throws InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		OverwriteWriteCommand writeCommand = new OverwriteWriteCommand(fileSystem);
		File file = Mockito.mock(File.class);
		fileSystem.currentFolder.fileChildren.add(file);
		Mockito.when(file.getName()).thenReturn("file.txt");
		writeCommand.executeWriteCommand("-overwrite file.txt s hello");
	}

	@Test
	public void givenOverwriteWriteCommandWithEmptyFileWhenOverwriteWriteCommandInvokedWithFileNameThenWriteToFile()
			throws DuplicateVirtualFileNameException, InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		OverwriteWriteCommand writeCommand = new OverwriteWriteCommand(fileSystem);
		DefaultCreateFileCommand createFileCommand = new DefaultCreateFileCommand(fileSystem);
		createFileCommand.executeCreateFileCommand("file.txt");
		writeCommand.executeWriteCommand("-overwrite file.txt 2 file content");
		assertEquals("\r\nfile content", fileSystem.findFile("file.txt").getContent());
	}

	@Test
	public void givenOverwriteWriteCommandWithNonEmptyFileWhenOverwriteWriteCommandInvokedWithFileNameThenOverwriteFile()
			throws DuplicateVirtualFileNameException, InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		OverwriteWriteCommand writeCommand = new OverwriteWriteCommand(fileSystem);
		DefaultCreateFileCommand createFileCommand = new DefaultCreateFileCommand(fileSystem);
		createFileCommand.executeCreateFileCommand("file.txt");
		File file = fileSystem.findFile("file.txt");
		file.setContent("hel", true);
		writeCommand.executeWriteCommand("-overwrite file.txt 0 lo world");
		assertEquals("lo world", file.getContent());
	}

	@Test(expected = NoSuchVirtualFileException.class)
	public void givenOverwriteWriteCommandWhenOverwriteWriteCommandInvokedWithRemovedFileNameThenThrowNoSuchVirtualFileException()
			throws DuplicateVirtualFileNameException, InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		OverwriteWriteCommand writeCommand = new OverwriteWriteCommand(fileSystem);
		DefaultCreateFileCommand createFileCommand = new DefaultCreateFileCommand(fileSystem);
		DefaultRmCommand rmCommand = new DefaultRmCommand(fileSystem);
		createFileCommand.executeCreateFileCommand("file.txt");
		rmCommand.executeRmCommand("file.txt");
		writeCommand.executeWriteCommand("-overwrite file.txt 2 hello");
	}

}
