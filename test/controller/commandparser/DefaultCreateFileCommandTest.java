package controller.commandparser;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mockito;

import exceptions.DuplicateVirtualFileNameException;
import model.FileSystem;
import model.File;

public class DefaultCreateFileCommandTest {

	@Test
	public void givenDefaultCreateFileCommandWhenExecuteCreateFileCommandInvokedWithNewNameThenCreateNewFile()
			throws DuplicateVirtualFileNameException {
		FileSystem fileSystem = new FileSystem();
		DefaultCreateFileCommand createFileCommand = new DefaultCreateFileCommand(fileSystem);
		createFileCommand.executeCreateFileCommand("file.txt");
		assertEquals(3, fileSystem.getNumberOfNodes());
		assertEquals("file.txt", fileSystem.currentFolder.fileChildren.get(0).getName());
	}

	@Test(expected = DuplicateVirtualFileNameException.class)
	public void givenDefaultCreateFileWhenExecuteCreateFileCommandInvokedWithExistingFileNameThenThrowException()
			throws DuplicateVirtualFileNameException {
		FileSystem fileSystem = new FileSystem();
		DefaultCreateFileCommand createFileCommand = new DefaultCreateFileCommand(fileSystem);
		File file = Mockito.mock(File.class);
		Mockito.when(file.getName()).thenReturn("file1.txt");
		fileSystem.currentFolder.fileChildren.add(file);
		createFileCommand.executeCreateFileCommand("file1.txt");
	}

	@Test
	public void givenDefaultCreateFileWhenExecuteCreateFileCommandInvokedWithIncorrectNameThenReturnWithoutCreatingNewName()
			throws DuplicateVirtualFileNameException {
		FileSystem fileSystem = new FileSystem();
		DefaultCreateFileCommand createFileCommand = new DefaultCreateFileCommand(fileSystem);
		createFileCommand.executeCreateFileCommand("file.tx");
		assertEquals(2, fileSystem.getNumberOfNodes());
	}

	@Test
	public void givenDefaultCreateFileWhenExecuteCreateFileCommandInvokedWithExistingFolderThenReturnNewFile()
			throws DuplicateVirtualFileNameException {
		FileSystem fileSystem = new FileSystem();
		DefaultMkdirCommand mkdirCommand = new DefaultMkdirCommand(fileSystem);
		mkdirCommand.executeMkdirCommand("name");
		DefaultCreateFileCommand createFileCommand = new DefaultCreateFileCommand(fileSystem);
		createFileCommand.executeCreateFileCommand("name.txt");
		assertEquals(4, fileSystem.getNumberOfNodes());
	}
}
