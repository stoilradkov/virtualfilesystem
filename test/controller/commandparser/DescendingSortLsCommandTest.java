package controller.commandparser;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;
import org.mockito.Mockito;

import model.File;
import model.FileSystem;

public class DescendingSortLsCommandTest {

	@Test
	public void givenDescendingSortLsCommandWithNoFilesWhenDescendingSortLsCommandInvokedThenPrintEmptyString() {
		FileSystem fileSystem = new FileSystem();
		DescendingSortLsCommand lsCommand = new DescendingSortLsCommand(fileSystem);
		ByteArrayOutputStream contentToBePrinted = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(contentToBePrinted);
		PrintStream old = System.out;
		System.setOut(ps);
		lsCommand.executeLsCommand("--sorted desc");
		System.out.flush();
		assertEquals("", contentToBePrinted.toString());
		System.setOut(old);
	}

	@Test
	public void givenDescendingSortLsCommandWithFilesWhenDescendingSortLsIvokedThenPrintFileNamesInDescendingOrder() {
		FileSystem fileSystem = new FileSystem();
		DescendingSortLsCommand lsCommand = new DescendingSortLsCommand(fileSystem);
		File file1 = Mockito.mock(File.class);
		File file2 = Mockito.mock(File.class);
		File file3 = Mockito.mock(File.class);
		fileSystem.currentFolder.fileChildren.add(file1);
		fileSystem.currentFolder.fileChildren.add(file2);
		fileSystem.currentFolder.fileChildren.add(file3);
		Mockito.when(file1.getName()).thenReturn("file1.txt");
		Mockito.when(file1.isRemoved()).thenReturn(false);
		Mockito.when(file1.getFileSize()).thenReturn(5);
		Mockito.when(file2.getName()).thenReturn("file2.txt");
		Mockito.when(file2.isRemoved()).thenReturn(false);
		Mockito.when(file2.getFileSize()).thenReturn(6);
		Mockito.when(file3.getName()).thenReturn("file3.txt");
		Mockito.when(file3.isRemoved()).thenReturn(true);
		Mockito.when(file3.getFileSize()).thenReturn(4);
		ByteArrayOutputStream contentToBePrinted = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(contentToBePrinted);
		PrintStream old = System.out;
		System.setOut(ps);
		lsCommand.executeLsCommand("--sorted desc");
		System.out.flush();
		assertEquals("file2.txt\r\nfile1.txt", contentToBePrinted.toString().trim());
		System.setOut(old);
	}

}
