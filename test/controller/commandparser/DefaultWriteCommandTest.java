package controller.commandparser;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mockito;

import exceptions.DuplicateVirtualFileNameException;
import exceptions.InvalidCommandException;
import exceptions.NoSuchVirtualFileException;
import model.File;
import model.FileSystem;

public class DefaultWriteCommandTest {

	@Test(expected = NoSuchVirtualFileException.class)
	public void givenDefaultWriteCommandWithFileWhenDefaultWriteCommandInvokedWithNonExistentFileNameThenThrowNoSuchVirtualFileException()
			throws InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultWriteCommand writeCommand = new DefaultWriteCommand(fileSystem);
		File file = Mockito.mock(File.class);
		fileSystem.currentFolder.fileChildren.add(file);
		Mockito.when(file.getName()).thenReturn("file.txt");
		writeCommand.executeWriteCommand("file1.txt 2 hello");
	}

	@Test(expected = InvalidCommandException.class)
	public void givenDefaultWriteCommandWhenDefaultWriteCommandInvokedWithIncorrectSyntaxThenThrowInvalidCommandException()
			throws InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultWriteCommand writeCommand = new DefaultWriteCommand(fileSystem);
		writeCommand.executeWriteCommand("file.txt 2 ");
	}

	@Test(expected = NumberFormatException.class)
	public void givenDefaultWriteCommandWhenDefaultWriteCommandInvokedWithWrongLineNumberThenThrowNumberFormatException()
			throws InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultWriteCommand writeCommand = new DefaultWriteCommand(fileSystem);
		File file = Mockito.mock(File.class);
		fileSystem.currentFolder.fileChildren.add(file);
		Mockito.when(file.getName()).thenReturn("file.txt");
		writeCommand.executeWriteCommand("file.txt s hello");
	}

	@Test
	public void givenDefaultWriteCommandWithEmptyFileWhenDefaultWriteCommandInvokedWithFileNameThenWriteToFile()
			throws DuplicateVirtualFileNameException, InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultWriteCommand writeCommand = new DefaultWriteCommand(fileSystem);
		DefaultCreateFileCommand createFileCommand = new DefaultCreateFileCommand(fileSystem);
		createFileCommand.executeCreateFileCommand("file.txt");
		writeCommand.executeWriteCommand("file.txt 2 file content");
		assertEquals("\r\nfile content", fileSystem.findFile("file.txt").getContent());
	}

	@Test
	public void givenDefaultWriteCommandWithNonEmptyFileWhenDefaultWriteCommandInvokedWithFileNameThenAppendToFile()
			throws DuplicateVirtualFileNameException, InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultWriteCommand writeCommand = new DefaultWriteCommand(fileSystem);
		DefaultCreateFileCommand createFileCommand = new DefaultCreateFileCommand(fileSystem);
		createFileCommand.executeCreateFileCommand("file.txt");
		File file = fileSystem.findFile("file.txt");
		file.setContent("hel", true);
		writeCommand.executeWriteCommand("file.txt 0 lo world");
		assertEquals("hello world", file.getContent());
	}

	@Test(expected = NoSuchVirtualFileException.class)
	public void givenDefaultWriteCommandWhenDefaultWriteCommandInvokedWithRemovedFileNameThenThrowNoSuchVirtualFileException()
			throws DuplicateVirtualFileNameException, InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultWriteCommand writeCommand = new DefaultWriteCommand(fileSystem);
		DefaultCreateFileCommand createFileCommand = new DefaultCreateFileCommand(fileSystem);
		DefaultRmCommand rmCommand = new DefaultRmCommand(fileSystem);
		createFileCommand.executeCreateFileCommand("file.txt");
		rmCommand.executeRmCommand("file.txt");
		writeCommand.executeWriteCommand("file.txt 0 hello");
	}
}
