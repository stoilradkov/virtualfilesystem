package controller.commandparser;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.FileSystem;
import model.Folder;

public class DefaultCdCommandTest {
	DefaultCdCommand cdCommand;
	FileSystem fileSystem;

	@Before
	public void setup() {
		fileSystem = new FileSystem();
		cdCommand = new DefaultCdCommand(fileSystem);
	}

	@Test
	public void givenDefaultCdCommandWhenExecuteCdCommandInvokedWithRootDirThenReturnToRoot() {
		cdCommand.executeCdCommand("/");
		assertEquals("/", fileSystem.getCurrentLocation());
		assertEquals(null, fileSystem.getFolderParent());
	}

	@Test
	public void givenDefaultCdCommandWhenExecuteCdCommandInvokedWithTwoDotsThenGoBackOnce() {
		cdCommand.executeCdCommand("..");
		assertEquals("/", fileSystem.getCurrentLocation());
		assertEquals(null, fileSystem.getFolderParent());
	}

	@Test
	public void givenDefaultCdCommandWhenExecuteCdCommandInvokedWithOneDotThenDoNothing() {
		cdCommand.executeCdCommand(".");
		assertEquals("/home", fileSystem.getCurrentLocation());
		assertNotEquals(null, fileSystem.getFolderParent());
	}

	@Test
	public void givenDefaultCdCommandWhenExecuteCdCommandInvokedWithFullPathThenGoToDir() {
		cdCommand.executeCdCommand("/home");
		assertEquals("/home", fileSystem.getCurrentLocation());
	}

	@Test
	public void givenDefaultCdCommandWhenExecuteCdCommandInvokedWithExistingFolderThenGoToFolder() {
		fileSystem.currentFolder.addFolderChild(new Folder("folder"));
		cdCommand.executeCdCommand("folder");
		assertEquals("/home/folder", fileSystem.getCurrentLocation());
	}

	@Test
	public void givenDefaultCdCommandWhenExecuteCdCommandInvokedWithInvalidPathThenDoNothing() {
		cdCommand.executeCdCommand("Invalid path");
		assertEquals("/home", fileSystem.getCurrentLocation());
	}
}
