package controller.commandparser;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import exceptions.InvalidCommandException;
import exceptions.NoSuchVirtualFileException;
import model.File;
import model.FileSystem;

public class DefaultRemoveCommandTest {

	@Test(expected = InvalidCommandException.class)
	public void givenDefaultRemoveCommandWhenExecuteRemoveCommandInvokedWithIncorrectInputThenThrowInvalidCommandException()
			throws InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultRemoveCommand removeCommand = new DefaultRemoveCommand(fileSystem);
		removeCommand.executeRemoveCommand("file.txt 1 2");
	}

	@Test(expected = NoSuchVirtualFileException.class)
	public void givenDefaultRemoveCommandWithNoFilesWhenExecuteRemoveCommandInvokedWithFileNameThenThrowNoSuchVirtualFileException()
			throws InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultRemoveCommand removeCommand = new DefaultRemoveCommand(fileSystem);
		removeCommand.executeRemoveCommand("file.tx 1-2");
	}

	@Test(expected = NoSuchVirtualFileException.class)
	public void givenDefaultRemoveCommandWithFilesWhenExecuteRemoveCommandInvokedWithRemoveFileThenThrowNoSuchVirtualFileException()
			throws InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultRemoveCommand removeCommand = new DefaultRemoveCommand(fileSystem);
		File file = new File("file.txt");
		fileSystem.currentFolder.fileChildren.add(file);
		file.setContent("a\r\nb\r\nc", true);
		DefaultRmCommand rmCommand = new DefaultRmCommand(fileSystem);
		rmCommand.executeRmCommand("file.txt");
		removeCommand.executeRemoveCommand("file.txt 0-3");
	}

	@Test(expected = InvalidCommandException.class)
	public void givenDefaultRemoveCommandWithFilesWhenExecuteRemoveCommandInvokedWithIncorrectLineNumbersSyntaxThenThrowInvalidCommandException()
			throws InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultRemoveCommand removeCommand = new DefaultRemoveCommand(fileSystem);
		File file = new File("file.txt");
		fileSystem.currentFolder.fileChildren.add(file);
		removeCommand.executeRemoveCommand("file.txt 1:2");
	}

	@Test(expected = NumberFormatException.class)
	public void givenDefaultRemoveCommandWithFilesWhenExecuteRemoveCommandInvokedWithStringLineNumbersSyntaxThenThrowNumberFormatException()
			throws InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultRemoveCommand removeCommand = new DefaultRemoveCommand(fileSystem);
		File file = new File("file.txt");
		fileSystem.currentFolder.fileChildren.add(file);
		removeCommand.executeRemoveCommand("file.txt a-b");
	}

	@Test(expected = InvalidCommandException.class)
	public void givenDefaultRemoveCommandWithFilesWhenExecuteRemoveCommandInvokedWithNegativeLineNumbersThenThrowInvalidCommandException()
			throws InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultRemoveCommand removeCommand = new DefaultRemoveCommand(fileSystem);
		File file = new File("file.txt");
		fileSystem.currentFolder.fileChildren.add(file);
		removeCommand.executeRemoveCommand("file.txt -1-2");
	}

	@Test(expected = InvalidCommandException.class)
	public void givenDefaultRemoveCommandWithFilesWhenExecuteRemoveCommandInvokedWithFromLineNumberBiggerThanToLineNumberThenThrowInvalidCommandException()
			throws InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultRemoveCommand removeCommand = new DefaultRemoveCommand(fileSystem);
		File file = new File("file.txt");
		fileSystem.currentFolder.fileChildren.add(file);
		removeCommand.executeRemoveCommand("file.txt 3-1");
	}

	@Test(expected = InvalidCommandException.class)
	public void givenDefaultRemoveCommandWithFilesWhenExecuteRemoveCommandInvokedWithFromLineNumberBiggerTotalLinesNumberThenThrowInvalidCommandException()
			throws InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultRemoveCommand removeCommand = new DefaultRemoveCommand(fileSystem);
		File file = new File("file.txt");
		fileSystem.currentFolder.fileChildren.add(file);
		file.setContent("a\r\nb\r\nc", true);
		removeCommand.executeRemoveCommand("file.txt 3-4");
	}

	@Test
	public void givenDefaultRemoveCommandWithFilesWhenExecuteRemoveCommandInvokedWithToLineEqualToTotalLinesThenRemoveLines()
			throws InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultRemoveCommand removeCommand = new DefaultRemoveCommand(fileSystem);
		File file = new File("file.txt");
		fileSystem.currentFolder.fileChildren.add(file);
		file.setContent("a\r\nb\r\nc", true);
		removeCommand.executeRemoveCommand("file.txt 1-3");
		assertEquals("a", file.getContent());
	}

	@Test
	public void givenDefaultRemoveCommandWithFilesWhenExecuteRemoveCommandInvokedWithFromLineEqualToZeroAndToLineEqualToTotalLinesThenRemoveWholeContent()
			throws InvalidCommandException, NoSuchVirtualFileException {
		FileSystem fileSystem = new FileSystem();
		DefaultRemoveCommand removeCommand = new DefaultRemoveCommand(fileSystem);
		File file = new File("file.txt");
		fileSystem.currentFolder.fileChildren.add(file);
		file.setContent("a\r\nb\r\nc", true);
		removeCommand.executeRemoveCommand("file.txt 0-3");
		assertEquals("", file.getContent());
	}

}
