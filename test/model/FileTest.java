package model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FileTest {

	File file;

	@Before
	public void setup() {
		file = new File("fileName");
	}

	@Test
	public void givenEmptyFileWhenSetContentInvokedThenSetCorrectFileSize() {
		file.setContent("/r/n/r/nasd", true);
		assertEquals(11, file.getFileSize());
	}

	@Test
	public void givenNonEmptyFileWhenSetContentInvokedWithOutOverwriteThenSetCorrectFileSize() {
		file.setContent("name", true);
		file.setContent("/r/n/r/nasd", false);
		assertEquals(15, file.getFileSize());
	}

	@Test
	public void givenNonEmptyFileWhenSetContentInvokedWithOverwriteThenOverwriteFile() {
		file.setContent("/r/n/r/nasd", false);
		file.setContent("content", true);
		assertEquals(7, file.getFileSize());
		assertEquals("content", file.getContent());
	}

}
