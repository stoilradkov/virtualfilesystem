package model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FileSystemTest {

	FileSystem fileSystem;

	@Before
	public void setup() {
		fileSystem = new FileSystem();
	}

	@Test
	public void givenEmptyFileSystemWhenFileSystemConstructorInvokedThenInitializeTwoNodes() {
		assertEquals(2, fileSystem.getNumberOfNodes());
	}

	@Test
	public void givenEmptyFileSystemWhenFileSystemConstructorInvokedThenInitializeCurrentLocation() {
		FileSystem fileSystem = new FileSystem();
		assertEquals("/home", fileSystem.getCurrentLocation());
	}

	@Test
	public void givenEmptyFileSystemWhenBackCurrentFolderOnceInvokedThenSetCurrentToItsParent() {
		fileSystem.backCurrentFolderOnce();
		assertEquals("/", fileSystem.getCurrentLocation());
		assertEquals(null, fileSystem.getFolderParent());
	}

	@Test
	public void givenEmptyFileSystemInRootWhenBackCurrentFolderOnceInvokedThenDoNothing() {
		fileSystem.backCurrentFolderOnce();
		fileSystem.backCurrentFolderOnce();
		assertEquals("/", fileSystem.getCurrentLocation());
		assertEquals(null, fileSystem.getFolderParent());
	}
}
